import argparse

import glob

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from PIL import ImageFilter

# image: base image to give a drop shadow
# iterations: number of times to apply the blur filter to the shadow
# border: border to give the image to leave space for the shadow
# offset: offset of the shadow as [x,y]
# backgroundCOlour: colour of the background
# shadowColour: colour of the drop shadow
def makeShadow(image, iterations, border, offset, backgroundColour, shadowColour):
    #Calculate the size of the shadow's image
    fullWidth  = image.size[0] + abs(offset[0]) + 2*border
    fullHeight = image.size[1] + abs(offset[1]) + 2*border

    #Create the shadow's image. Match the parent image's mode.
    shadow = Image.new(image.mode, (fullWidth, fullHeight), backgroundColour)

    # Place the shadow, with the required offset
    shadowLeft = border + max(offset[0], 0) #if <0, push the rest of the image right
    shadowTop  = border + max(offset[1], 0) #if <0, push the rest of the image down
    #Paste in the constant colour
    shadow.paste(shadowColour,
                [shadowLeft, shadowTop,
                 shadowLeft + image.size[0],
                 shadowTop  + image.size[1] ])

    # Apply the BLUR filter repeatedly
    for i in range(iterations):
        shadow = shadow.filter(ImageFilter.BLUR)

    # Paste the original image on top of the shadow
    imgLeft = border - min(offset[0], 0) #if the shadow offset was <0, push right
    imgTop  = border - min(offset[1], 0) #if the shadow offset was <0, push down
    shadow.paste(image, (imgLeft, imgTop))

    return shadow

class Range(argparse.Action):
    def __init__(self, min=None, max=None, *args, **kwargs):
        self.min = min
        self.max = max
        kwargs["metavar"] = "[%d-%d]" % (self.min, self.max)
        super(Range, self).__init__(*args, **kwargs)

    def __call__(self, parser, namespace, value, option_string=None):
        if not (self.min <= value <= self.max):
            msg = 'invalid choice: %r (choose from [%d-%d])' % (value, self.min, self.max)
            raise argparse.ArgumentError(self, msg)
        setattr(namespace, self.dest, value)

parser = argparse.ArgumentParser()
parser.add_argument("-w", "--width", help="Sets the total width of the image", type=int, default=500, min=0, max=10000, action=Range)
parser.add_argument("-b", "--border", help="Sets the border width [%(min)d-%(max)d]", type=int, default=30, min=0, max=100, action=Range)
parser.add_argument("-s", "--space", help="Sets the width of the space between the images", type=int, default=40, min=0, max=100, action=Range)
parser.add_argument("-o", "--output", help="Sets the output image name", type=str, default="output.png")
parser.add_argument("-i", "--input", help="Sets the input folder name", type=str, default="images")
args = parser.parse_args()

images = []

shadow_border = 10

types = ('*.jpg', '*.jpeg', '*.png')

for type in types:
    for filename in glob.glob(args.input+'/'+type):
        im = Image.open(filename)

        if im is None:
            print("Failed to open \"{}\", skipping.".format(filename))
        else:
            wpercent = (args.width / float(im.width))
            height = int((float(im.height) * float(wpercent)))

            with_shadow = makeShadow(im, 20, shadow_border, (0, 0), (255, 255, 255), (100, 100, 100));
            resized = with_shadow.resize((args.width, height))

            images.append(resized)

# Error: no images in that folder
if len(images) == 0:
    print("Error: no images found in \"{}\" folder".format(args.input))
    quit(1)

total_height = sum([image.height for image in images]) + (len(images)-1)*args.space + 2*args.border

y = args.border
i = 1
dst = Image.new('RGB', (args.width + (2*args.border), total_height), (255, 255, 255))
font = ImageFont.truetype("MarkerFelt.ttc", 25)
draw = ImageDraw.Draw(dst)

for image in images:
    dst.paste(image, (args.border, y))
    draw.text((args.border+shadow_border/2, y-args.space/2),str(i),(0,0,0), font=font)
    y = y + image.height + args.space
    i = i+1

dst.save(args.output)

print("Result saved into \"{}\"".format(args.output))
